# AoC 2023 solutions of mine

Just a regular cargo project. Follow [Rust book](https://doc.rust-lang.org/book/). 


You can build by

```sh
cargo build

```

Test (if there will be any tests 😄)

``` sh
cargo test
```

or run any given puzzle like (1st december, part 1:

``` sh
cargo run --example 1_1
```
