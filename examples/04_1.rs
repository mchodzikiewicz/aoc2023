fn main() {
    let input = include_str!("../inputs/input04.txt");
    let res: u32 = input
        .lines()
        .map(|line| {
            line.split_once(':')
                .unwrap()
                .1
                .split_once('|')
                .map(|(card, winners)| {
                    (
                        card.split_whitespace()
                            .map(|n| n.parse::<usize>().unwrap())
                            .collect::<Vec<_>>(),
                        winners
                            .split_whitespace()
                            .map(|n| n.parse::<usize>().unwrap())
                            .collect::<Vec<_>>(),
                    )
                })
                .unwrap()
        })
        .fold(0, |acc, (card, winners)| {
            let mut points = 0;
            for n in card {
                for w in &winners {
                    if n == *w {
                        if points > 0 {
                            points = points * 2;
                        } else {
                            points = 1;
                        }
                    }
                }
            }
            acc + points
        });
    println!("res: {res:?}");
}
