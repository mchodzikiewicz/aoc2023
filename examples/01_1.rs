fn main() {
    let input = include_str!("../inputs/input01.txt");
    let res: u32 = input
        .lines()
        .map(|line| {
            let vals: Vec<char> = line.chars().filter(|c| c.is_ascii_digit()).collect();
            vals.first().unwrap().to_digit(10).unwrap() * 10
                + vals.last().unwrap().to_digit(10).unwrap()
        })
        .sum();
    println!("res: {res:?}");
}
