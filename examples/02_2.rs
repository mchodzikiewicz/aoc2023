#[derive(Debug)]
struct CubeSet {
    red: usize,
    green: usize,
    blue: usize,
}

fn main() {
    let input = include_str!("../inputs/input02.txt");
    let res: usize = input
        .lines()
        .map(|line| {
            line.split(":")
                .last()
                .unwrap()
                .split(';')
                .map(|cubes| {
                    cubes
                        .split(',')
                        .map(|cube| {
                            let mut cube = cube.trim().split_whitespace();
                            (
                                cube.clone().last().unwrap(),
                                cube.next().unwrap().parse::<usize>().unwrap(),
                            )
                        })
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
        })
        .map(|games| {
            let mut minimal_set = CubeSet {
                red: 0,
                green: 0,
                blue: 0,
            };
            for game in games {
                for (color, val) in game {
                    match color {
                        "red" => {
                            if val > minimal_set.red {
                                minimal_set.red = val;
                            }
                        }
                        "green" => {
                            if val > minimal_set.green {
                                minimal_set.green = val;
                            }
                        }
                        "blue" => {
                            if val > minimal_set.blue {
                                minimal_set.blue = val;
                            }
                        }
                        _ => panic!("Unknown color!"),
                    };
                }
            }
            minimal_set.red * minimal_set.green * minimal_set.blue
        })
        .sum();
    println!("res: {res:?}");
}
