const LUT: &[(&str, u32)] = &[
    ("1", 1),
    ("2", 2),
    ("3", 3),
    ("4", 4),
    ("5", 5),
    ("6", 6),
    ("7", 7),
    ("8", 8),
    ("9", 9),
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
];

fn main() {
    let input = include_str!("../inputs/input01.txt");
    let res = input
        .lines()
        .map(|line| {
            let mut v = Vec::new();
            for i in 0..line.len() {
                for (symbol, val) in LUT {
                    if line[i..].starts_with(symbol) {
                        v.push(*val);
                    }
                }
            }
            v.first().unwrap() * 10 + v.last().unwrap()
        })
        .sum::<u32>();
    println!("res: {res}");
}
