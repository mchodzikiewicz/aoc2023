use std::collections::VecDeque;

fn main() {
    let input = include_str!("../inputs/input04.txt");
    let res: u32 = input
        .lines()
        .map(|line| {
            line.split_once(':')
                .unwrap()
                .1
                .split_once('|')
                .map(|(card, winners)| {
                    (
                        card.split_whitespace()
                            .map(|n| n.parse::<usize>().unwrap())
                            .collect::<Vec<_>>(),
                        winners
                            .split_whitespace()
                            .map(|n| n.parse::<usize>().unwrap())
                            .collect::<Vec<_>>(),
                    )
                })
                .unwrap()
        })
        .fold((0, VecDeque::new()), |(acc, mut vec), (card, winners)| {
            let mut matches = 0;
            let current_card_count = vec.pop_front().unwrap_or(0) + 1;
            for n in card {
                for w in &winners {
                    if n == *w {
                        matches += 1;
                    }
                }
            }
            for i in 0..matches {
                if vec.len() > i {
                    vec[i] = vec[i] + current_card_count;
                } else {
                    vec.push_back(current_card_count);
                }
            }
            (acc + current_card_count, vec)
        })
        .0;
    println!("res: {res:?}");
}
