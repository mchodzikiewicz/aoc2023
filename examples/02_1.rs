const R: usize = 12;
const G: usize = 13;
const B: usize = 14;

fn main() {
    let input = include_str!("../inputs/input02.txt");
    let res: usize = input
        .lines()
        .enumerate()
        .map(|(n, line)| {
            (
                n,
                line.split(":")
                    .last()
                    .unwrap()
                    .split(';')
                    .map(|cubes| {
                        cubes
                            .split(',')
                            .map(|cube| {
                                let mut cube = cube.trim().split_whitespace();
                                (
                                    cube.clone().last().unwrap(),
                                    cube.next().unwrap().parse::<usize>().unwrap(),
                                )
                            })
                            .collect::<Vec<_>>()
                    })
                    .collect::<Vec<_>>(),
            )
        })
        .filter_map(|(n, games)| {
            for game in games {
                for (color, val) in game {
                    match color {
                        "red" => {
                            if val > R {
                                return None;
                            }
                        }
                        "green" => {
                            if val > G {
                                return None;
                            }
                        }
                        "blue" => {
                            if val > B {
                                return None;
                            }
                        }
                        _ => panic!("Unknown color!"),
                    };
                }
            }
            Some(n + 1)
        })
        .sum();
    println!("res: {res:?}");
}
