use std::ops::Range;

#[derive(Debug)]
struct Translation {
    source: Range<i32>,
    offset: i32,
}

fn main() {
    let input = include_str!("../inputs/input05.txt");
    let (seeds, translators) = input
        .split_once("\n\n")
        .map(|(a, b)| {
            (
                a[7..]
                    .split_whitespace()
                    .map(|number| number.parse::<i32>().unwrap())
                    .collect::<Vec<_>>(),
                b.split("\n\n")
                    .map(|chunk| {
                        chunk
                            .lines()
                            .skip(1)
                            .map(|line| {
                                line.split_whitespace()
                                    .map(|number| number.parse::<i32>().unwrap())
                                    .collect::<Vec<_>>()
                            })
                            .map(|numbers| Translation {
                                source: numbers[1]..numbers[1] + numbers[2],
                                offset: numbers[0] - numbers[1],
                            })
                            .collect::<Vec<_>>()
                    })
                    .collect::<Vec<_>>(),
            )
        })
        .unwrap();

    let mut seeds_translated = seeds.clone();
    for translator in &translators {
        let mut seeds_translated_tmp = seeds_translated.clone();
        println!("iter mapping: {:?}", seeds_translated);
        for unit in translator {
            for i in 0..seeds_translated.len() {
                if unit.source.contains(&seeds_translated[i]) {
                    seeds_translated_tmp[i] = seeds_translated[i] + unit.offset;
                }
            }
        }
        seeds_translated = seeds_translated_tmp;
    }

    let res = (seeds_translated.iter().min(), translators);
    println!("res: {res:?}");
}
